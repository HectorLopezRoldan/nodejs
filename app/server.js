const express = require('express');
var app = express();
require('./config/db')//para mongo

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


const router = require('./routes.js');
const router2 = require('./routes2.js');
var port = process.env.PORT || 8080 ; // establecemos nuestro puerto



// nuestra ruta irá en http://localhost:8080/api
// es bueno que haya un prefijo, sobre todo por el tema de versiones de la API
app.use('/api', router);
app.use('/api2', router2);

// iniciamos nuestro servidor
app.listen(port);
console.log('API escuchando en el puerto ' + port);