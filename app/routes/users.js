const express = require('express');
const router = express.Router();
const userController =require('../controllers/userController.js');

router.get('/', userController.index);

router.get('/:id',(req,res)=>{
  console.log('ruta show')
  userController.show(req,res);
});

router.delete('/:id',(req,res)=>{
  console.log('ruta delete')
  userController.destroy(req,res);
});

 router.post('/',(req,res)=>{
  console.log('ruta store')
  userController.store(req,res);
});

 router.put('/:id',(req,res)=>{
  console.log('ruta update')
  userController.update(req,res);
});

module.exports = router;