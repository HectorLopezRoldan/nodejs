const express = require('express');
const router = express.Router();
const cervezaController =require('../../controllers/v2/cervezaController.js');

  router.get('/',(req,res) =>{
    cervezaController.index(req,res);
    })
    router.get('/search', (req, res) => {
      cervezaController.search(req,res);
    })

    router.get('/:id', (req, res) => {
      cervezaController.show(req,res);
    })

    router.put('/:id', (req, res) => {
      cervezaController.create(req,res);
    })

    router.post('/', (req, res) => {
      cervezaController.create(req,res);
    })
    router.delete('/:id', (req, res) => {
      cervezaController.delete(req,res);
    })
  module.exports =router;