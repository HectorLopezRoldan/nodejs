const express = require('express');
const router = express.Router();
const cervezaController =require('../controllers/cervezaController.js');

  router.get('/', cervezaController.index);

  router.get('/:id',(req,res)=>{
    console.log('ruta show')
    cervezaController.show(req,res);
  });
  
  router.delete('/:id',(req,res)=>{
    console.log('ruta delete')
    cervezaController.destroy(req,res);
  });
  
   router.post('/',(req,res)=>{
    console.log('ruta store')
    cervezaController.store(req,res);
  });

   router.put('/:id',(req,res)=>{
    console.log('ruta update')
    cervezaController.update(req,res);
  });
  
  module.exports = router;