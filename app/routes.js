const express = require('express');
// para establecer las distintas rutas, necesitamos instanciar el express router
const router = express.Router()
const routerCervezas = require('./routes/cervezas.js');
const routerUsers = require('./routes/users.js');
// establecemos nuestra primera ruta, mediante get.
router.get('/', (req, res) => {
  res.json({ mensaje: '¡Bienvenido a nuestra API!' });
});

router.use('/cervezas', routerCervezas);
router.use('/users', routerUsers);

// para que se pueda acceder desde fuera
module.exports = router;
