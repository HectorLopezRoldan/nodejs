const User = require('../models/User.js');

const index = function (req, res) {
    console.log("en index user");
    User.index(function (status, data, fields) {
      res.status(status).json(data)
    })
  }
  
  const destroy = (req, res) => {
    const id = req.params.id
    User.destroy(id, function (status, data, fields) {
      res.status(status).json(data)
    })
  }
  const store = (req, res) => {
    User.store(req, function (status, data, fields) {
      res.status(status).json(data)
    })
  }
  
  const update = (req, res) => {
    User.update(req, function (status, data, fields) {
      res.status(status).json(data)
    })
  }
  
  const show = (req, res) => {
    const id = req.params.id
      User.show(id, function (status, data, fields) {
      res.status(status).json(data)
    })
  }
  
  module.exports = {
    index,
    destroy,
    store,
    update,
    show
  }
  