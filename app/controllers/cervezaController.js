const Cerveza = require('../models/Cerveza.js');

const index = function (req, res) {
   const cervezas = Cerveza.index(function(status,data,fields){
    res.status(status).json(data)
   });
}

const destroy = (req, res) => {
  const id = req.params.id
  const cervezas = Cerveza.destroy(id,function(status,data,fields){
    res.status(status).json(data)
   });
}

const store = (req, res) => {

    const cervezas = Cerveza.store(req,function(status,data,fields){
        res.status(status).json(data)
       });
}

const update = (req, res) => {
    const cervezas = Cerveza.update(req,function(status,data,fields){
        res.status(status).json(data)
       });
}

const show = (req, res) => {
  const id = req.params.id
  const cervezas = Cerveza.show(id,function(status,data,fields){
    res.status(status).json(data)
   });
}

module.exports = {
  index,
  destroy,
  store,
  update,
  show
}
