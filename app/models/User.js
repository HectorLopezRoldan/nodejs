const connection = require('../config/dbconnection.js')

const index = function(callback){

    const sql = 'select * from users'
    connection.query(sql, (err, result, fields) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
        callback(200, result, fields);
      }
    })

}

const destroy = function(id,callback){
    const sql = 'delete from users where id = ?'
    connection.query(sql, [id], (err, result, fields) => {
        if (err) {
            console.log(err);
        } else {
            console.log(result);
            callback(200, result, fields);
        }
      })
}

const show = function(id,callback){
    const sql = 'select * from users where id = ?'

    connection.query(sql, [id], (err, result, fields) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
        callback(200, result, fields);
      }
    })
}

const store = function(req,callback){
    const sql =
    'insert into users (role_id,name,email,password)' +
    'values (?,?,?,?)'

    connection.query(
    sql,
    [
      req.body.role_id,
      req.body.name,
      req.body.email,
      req.body.password
    ],
    (err, result, fields) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
        callback(200, result, fields);
      }
    }
  )
}

const update = function(req, callback){
    const id = req.params.id
    const sql =
      'UPDATE users SET role_id = ?, name = ?,email= ?,' +
      'password=?  WHERE id = ?'
  
    connection.query(
      sql,
      [
        req.body.role_id,
        req.body.name,
        req.body.email,
        req.body.password,
        id
      ],
      (err, result, fields) => {
        if (err) {
            console.log(err);
        } else {
            console.log(result);
            callback(200, result, fields);
        }
      }
    )
}


module.exports = {
    index,
    destroy,
    show,
    store,
    update
}