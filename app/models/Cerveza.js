const connection =require('../config/dbconnection.js');

const index = function(callback){

    const sql = 'select * from cervezas'
    connection.query(sql, (err, result, fields) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
        callback(200, result, fields);
      }
    })

}

const destroy = function(id,callback){
    const sql = 'delete from cervezas where id = ?'
    connection.query(sql, [id], (err, result, fields) => {
        if (err) {
            console.log(err);
        } else {
            console.log(result);
            callback(200, result, fields);
        }
      })
}

const show = function(id,callback){
    const sql = 'select * from cervezas where id = ?'

    connection.query(sql, [id], (err, result, fields) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
        callback(200, result, fields);
      }
    })
}

const store = function(req,callback){
    const sql =
    'insert into cervezas (name,description,container,alcohol,price)' +
    'values (?,?,?,?,?)'

  connection.query(
    sql,
    [
      req.body.name,
      req.body.descripcion,
      req.body.container,
      req.body.alcohol,
      req.body.price
    ],
    (err, result, fields) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
        callback(200, result, fields);
      }
    }
  )
}

const update = function(req, callback){
    const id = req.params.id
    const sql =
      'UPDATE cervezas SET name = ?, description = ?,alcohol= ?,' +
      'container=?, price = ?  WHERE id = ?'
  
    connection.query(
      sql,
      [
        req.body.name,
        req.body.descripcion,
        req.body.container,
        req.body.alcohol,
        req.body.price,
        id
      ],
      (err, result, fields) => {
        if (err) {
            console.log(err);
        } else {
            console.log(result);
            callback(200, result, fields);
        }
      }
    )
}


module.exports = {
    index,
    destroy,
    show,
    store,
    update
}
